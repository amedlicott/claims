package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.ClaimsService;
	
import com.digitalml.rest.resources.codegentest.service.ClaimsService.GetASpecificClaimReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.GetASpecificClaimReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.GetASpecificClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.FindClaimReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.FindClaimReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.FindClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.UpdateClaimReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.UpdateClaimReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.UpdateClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.ReplaceClaimReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.ReplaceClaimReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.ReplaceClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.CreateClaimReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.CreateClaimReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.CreateClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.DeleteClaimReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.DeleteClaimReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.DeleteClaimInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Claims
	 * The Claim is used by providers and payors, insurers, to exchange the financial information, and supporting clinical
information, regarding the provision of healthcare services with payors an firms which provide data analytics. The
primary uses of this resource is to support eClaims, the exchange of proposed or actual services to benefit payors,
insurers and national health programs, for treatment payment planning and reimbursement.
	 *
	 * @author admin
	 * @version 1.0
	 *
	 */
	
	@Path("http://digitalml.com/ICS/rest")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class ClaimsResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(ClaimsResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private ClaimsService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Claims.ClaimsServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private ClaimsService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof ClaimsService)) {
			LOGGER.error(implementationClass + " is not an instance of " + ClaimsService.class.getName());
			return null;
		}

		return (ClaimsService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getaspecificclaim
		Gets claim details

	Non-functional requirements:
	*/
	
	@GET
	@Path("/claim/{id}")
	public javax.ws.rs.core.Response getaspecificclaim(
		@PathParam("id")@NotEmpty String id) {

		GetASpecificClaimInputParametersDTO inputs = new ClaimsService.GetASpecificClaimInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			GetASpecificClaimReturnDTO returnValue = delegateService.getaspecificclaim(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findclaim
		Gets a collection of claim details filtered 

	Non-functional requirements:
	*/
	
	@GET
	@Path("/claim")
	public javax.ws.rs.core.Response findclaim(
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindClaimInputParametersDTO inputs = new ClaimsService.FindClaimInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindClaimReturnDTO returnValue = delegateService.findclaim(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateclaim
		Updates claim

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/claim/{id}")
	public javax.ws.rs.core.Response updateclaim(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		UpdateClaimInputParametersDTO inputs = new ClaimsService.UpdateClaimInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			UpdateClaimReturnDTO returnValue = delegateService.updateclaim(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replaceclaim
		Replaces claim

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/claim/{id}")
	public javax.ws.rs.core.Response replaceclaim(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		ReplaceClaimInputParametersDTO inputs = new ClaimsService.ReplaceClaimInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			ReplaceClaimReturnDTO returnValue = delegateService.replaceclaim(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createclaim
		Creates claim

	Non-functional requirements:
	*/
	
	@POST
	@Path("/claim")
	public javax.ws.rs.core.Response createclaim(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		CreateClaimInputParametersDTO inputs = new ClaimsService.CreateClaimInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			CreateClaimReturnDTO returnValue = delegateService.createclaim(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteclaim
		Deletes claim

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/claim/{id}")
	public javax.ws.rs.core.Response deleteclaim(
		@PathParam("id")@NotEmpty String id) {

		DeleteClaimInputParametersDTO inputs = new ClaimsService.DeleteClaimInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			DeleteClaimReturnDTO returnValue = delegateService.deleteclaim(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}