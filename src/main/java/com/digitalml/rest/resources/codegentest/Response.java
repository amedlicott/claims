package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "claim"
  ],
  "type": "object",
  "properties": {
    "claim": {
      "$ref": "Claim"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private com.digitalml.healthcare.financial.claim.Claim claim;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    claim = new com.digitalml.healthcare.financial.claim.Claim();
	}
	public com.digitalml.healthcare.financial.claim.Claim getClaim() {
		return claim;
	}
	
	public void setClaim(com.digitalml.healthcare.financial.claim.Claim claim) {
		this.claim = claim;
	}
}