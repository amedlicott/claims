package com.digitalml.healthcare.foundation.shared;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for AddressEHub:
{
  "type": "object",
  "properties": {
    "addressLineOne": {
      "type": "string"
    },
    "addressLineTwo": {
      "type": "string"
    },
    "addressLineThree": {
      "type": "string"
    },
    "city": {
      "type": "string"
    },
    "county": {
      "type": "string"
    },
    "state": {
      "type": "string"
    },
    "zipcode": {
      "type": "string"
    },
    "zipcodeExtension": {
      "type": "string"
    },
    "country": {
      "type": "string"
    },
    "type": {
      "type": "string"
    }
  }
}
*/

public class AddressEHub {

	@Size(max=1)
	private String addressLineOne;

	@Size(max=1)
	private String addressLineTwo;

	@Size(max=1)
	private String addressLineThree;

	@Size(max=1)
	private String city;

	@Size(max=1)
	private String county;

	@Size(max=1)
	private String state;

	@Size(max=1)
	private String zipcode;

	@Size(max=1)
	private String zipcodeExtension;

	@Size(max=1)
	private String country;

	@Size(max=1)
	private String type;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    addressLineOne = null;
	    addressLineTwo = null;
	    addressLineThree = null;
	    city = null;
	    county = null;
	    state = null;
	    zipcode = null;
	    zipcodeExtension = null;
	    country = null;
	    type = null;
	}
	public String getAddressLineOne() {
		return addressLineOne;
	}
	
	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}
	public String getAddressLineTwo() {
		return addressLineTwo;
	}
	
	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}
	public String getAddressLineThree() {
		return addressLineThree;
	}
	
	public void setAddressLineThree(String addressLineThree) {
		this.addressLineThree = addressLineThree;
	}
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	public String getCounty() {
		return county;
	}
	
	public void setCounty(String county) {
		this.county = county;
	}
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getZipcodeExtension() {
		return zipcodeExtension;
	}
	
	public void setZipcodeExtension(String zipcodeExtension) {
		this.zipcodeExtension = zipcodeExtension;
	}
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
}