package com.digitalml.healthcare.foundation.shared;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Amount:
{
  "type": "object",
  "properties": {
    "value": {
      "type": "number",
      "format": "double"
    },
    "units": {
      "type": "string"
    }
  }
}
*/

public class Amount {

	@Size(max=1)
	private Double value;

	@Size(max=1)
	private String units;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    units = null;
	}
	public Double getValue() {
		return value;
	}
	
	public void setValue(Double value) {
		this.value = value;
	}
	public String getUnits() {
		return units;
	}
	
	public void setUnits(String units) {
		this.units = units;
	}
}