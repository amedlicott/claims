package com.digitalml.healthcare.financial.claim;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Claim:
{
  "description": "CLAIM contains information about each claim at a header level",
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "sensitiveClaimIndicator": {
      "type": "boolean"
    },
    "securityLevelCode": {
      "type": "string"
    },
    "recievedDate": {
      "type": "string",
      "format": "date"
    },
    "startDate": {
      "type": "string",
      "format": "date"
    },
    "endDate": {
      "type": "string",
      "format": "date"
    },
    "groupIdentifier": {
      "type": "string"
    },
    "healthCardIdentifier": {
      "type": "string"
    },
    "memberIdentifier": {
      "type": "string"
    },
    "contractIdentifier": {
      "type": "string"
    },
    "productIdentifier": {
      "type": "string"
    },
    "patientRelationshipCode": {
      "type": "string"
    },
    "patientInfo": {
      "$ref": "Person"
    },
    "subscriberIdentifier": {
      "type": "string"
    },
    "subscriberInfo": {
      "$ref": "Person"
    },
    "subscriberAddress": {
      "$ref": "AddressEHub"
    },
    "totalAmounts": {
      "$ref": "ClaimAmountSet"
    },
    "behavioralHealthIndicator": {
      "type": "boolean"
    },
    "CDHPIndicator": {
      "description": "Whether this claim is related to a Consumer-Driven Health Plan.",
      "type": "boolean"
    },
    "ERISAIndicator": {
      "description": "Whether this claim is related to a group that is part of the Employment Retirement Security Income Act.",
      "type": "boolean"
    },
    "ITSIndicator": {
      "type": "boolean"
    },
    "splitClaimIndicator": {
      "type": "boolean"
    },
    "adjustmentIndicator": {
      "type": "boolean"
    },
    "revisionNumber": {
      "type": "integer",
      "format": "int32"
    },
    "lastProcessedTimestamp": {
      "type": "string",
      "format": "date-time"
    },
    "lastUpdatedTimestamp": {
      "type": "string",
      "format": "date-time"
    },
    "type": {
      "description": "institutional | oral | pharmacy | professional | vision",
      "type": "string"
    },
    "createdDateTime": {
      "description": "Creation date and time.",
      "type": "string",
      "format": "date-time"
    },
    "use": {
      "description": "complete | proposed | exploratory | other",
      "type": "string"
    },
    "priority": {
      "description": "Desired processing priority.",
      "type": "string"
    },
    "fundsReservation": {
      "description": "Funds requested to be reserved.",
      "type": "string"
    },
    "condition": {
      "description": "A presenting Condition.",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "exception": {
      "description": "An Eligibility exception.",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "schoolName": {
      "description": "Name of School",
      "type": "string"
    },
    "accidentDateTime": {
      "description": "Accident Date and Time.",
      "type": "string",
      "format": "date-time"
    },
    "accidentType": {
      "description": "Accident Type. Examples include Motor vehicle accident (MVA), School Accident (SCHOOL), Sporting Accident (SPT), Workplace Accident (WPA)",
      "type": "string"
    },
    "interventionException": {
      "description": "Intervention and exception code (Pharma).",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "additionalMaterials": {
      "description": "Additional materials, documents, etc. Examples: xray, model, document",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "actionCode": {
      "description": "CLAIM ACTION CODE values indicate what actions were taken on claim, which helps with the information on claim being either paid, or rejected.",
      "type": "string"
    },
    "actionDescription": {
      "description": "CLAIM ACTION DESCRIPTION is the textual description of the Claim Action Code values.",
      "type": "string"
    }
  }
}
*/

public class Claim {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private boolean sensitiveClaimIndicator;

	@Size(max=1)
	private String securityLevelCode;

	@Size(max=1)
	private Date recievedDate;

	@Size(max=1)
	private Date startDate;

	@Size(max=1)
	private Date endDate;

	@Size(max=1)
	private String groupIdentifier;

	@Size(max=1)
	private String healthCardIdentifier;

	@Size(max=1)
	private String memberIdentifier;

	@Size(max=1)
	private String contractIdentifier;

	@Size(max=1)
	private String productIdentifier;

	@Size(max=1)
	private String patientRelationshipCode;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Person patientInfo;

	@Size(max=1)
	private String subscriberIdentifier;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.Person subscriberInfo;

	@Size(max=1)
	private com.digitalml.healthcare.foundation.shared.AddressEHub subscriberAddress;

	@Size(max=1)
	private com.digitalml.healthcare.financial.claim.ClaimAmountSet totalAmounts;

	@Size(max=1)
	private boolean behavioralHealthIndicator;

	@Size(max=1)
	private boolean cDHPIndicator;

	@Size(max=1)
	private boolean eRISAIndicator;

	@Size(max=1)
	private boolean iTSIndicator;

	@Size(max=1)
	private boolean splitClaimIndicator;

	@Size(max=1)
	private boolean adjustmentIndicator;

	@Size(max=1)
	private Integer revisionNumber;

	@Size(max=1)
	private Date lastProcessedTimestamp;

	@Size(max=1)
	private Date lastUpdatedTimestamp;

	@Size(max=1)
	private String type;

	@Size(max=1)
	private Date createdDateTime;

	@Size(max=1)
	private String use;

	@Size(max=1)
	private String priority;

	@Size(max=1)
	private String fundsReservation;

	@Size(max=1)
	private List<String> condition;

	@Size(max=1)
	private List<String> exception;

	@Size(max=1)
	private String schoolName;

	@Size(max=1)
	private Date accidentDateTime;

	@Size(max=1)
	private String accidentType;

	@Size(max=1)
	private List<String> interventionException;

	@Size(max=1)
	private List<String> additionalMaterials;

	@Size(max=1)
	private String actionCode;

	@Size(max=1)
	private String actionDescription;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    
	    securityLevelCode = null;
	    
	    
	    
	    groupIdentifier = null;
	    healthCardIdentifier = null;
	    memberIdentifier = null;
	    contractIdentifier = null;
	    productIdentifier = null;
	    patientRelationshipCode = null;
	    person = new com.digitalml.healthcare.foundation.shared.Person();
	    subscriberIdentifier = null;
	    person = new com.digitalml.healthcare.foundation.shared.Person();
	    addressEHub = new com.digitalml.healthcare.foundation.shared.AddressEHub();
	    claimAmountSet = new com.digitalml.healthcare.financial.claim.ClaimAmountSet();
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    type = null;
	    
	    use = null;
	    priority = null;
	    fundsReservation = null;
	    condition = new ArrayList<String>();
	    exception = new ArrayList<String>();
	    schoolName = null;
	    
	    accidentType = null;
	    interventionException = new ArrayList<String>();
	    additionalMaterials = new ArrayList<String>();
	    actionCode = null;
	    actionDescription = null;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public boolean getSensitiveClaimIndicator() {
		return sensitiveClaimIndicator;
	}
	
	public void setSensitiveClaimIndicator(boolean sensitiveClaimIndicator) {
		this.sensitiveClaimIndicator = sensitiveClaimIndicator;
	}
	public String getSecurityLevelCode() {
		return securityLevelCode;
	}
	
	public void setSecurityLevelCode(String securityLevelCode) {
		this.securityLevelCode = securityLevelCode;
	}
	public Date getRecievedDate() {
		return recievedDate;
	}
	
	public void setRecievedDate(Date recievedDate) {
		this.recievedDate = recievedDate;
	}
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getGroupIdentifier() {
		return groupIdentifier;
	}
	
	public void setGroupIdentifier(String groupIdentifier) {
		this.groupIdentifier = groupIdentifier;
	}
	public String getHealthCardIdentifier() {
		return healthCardIdentifier;
	}
	
	public void setHealthCardIdentifier(String healthCardIdentifier) {
		this.healthCardIdentifier = healthCardIdentifier;
	}
	public String getMemberIdentifier() {
		return memberIdentifier;
	}
	
	public void setMemberIdentifier(String memberIdentifier) {
		this.memberIdentifier = memberIdentifier;
	}
	public String getContractIdentifier() {
		return contractIdentifier;
	}
	
	public void setContractIdentifier(String contractIdentifier) {
		this.contractIdentifier = contractIdentifier;
	}
	public String getProductIdentifier() {
		return productIdentifier;
	}
	
	public void setProductIdentifier(String productIdentifier) {
		this.productIdentifier = productIdentifier;
	}
	public String getPatientRelationshipCode() {
		return patientRelationshipCode;
	}
	
	public void setPatientRelationshipCode(String patientRelationshipCode) {
		this.patientRelationshipCode = patientRelationshipCode;
	}
	public com.digitalml.healthcare.foundation.shared.Person getPatientInfo() {
		return patientInfo;
	}
	
	public void setPatientInfo(com.digitalml.healthcare.foundation.shared.Person patientInfo) {
		this.patientInfo = patientInfo;
	}
	public String getSubscriberIdentifier() {
		return subscriberIdentifier;
	}
	
	public void setSubscriberIdentifier(String subscriberIdentifier) {
		this.subscriberIdentifier = subscriberIdentifier;
	}
	public com.digitalml.healthcare.foundation.shared.Person getSubscriberInfo() {
		return subscriberInfo;
	}
	
	public void setSubscriberInfo(com.digitalml.healthcare.foundation.shared.Person subscriberInfo) {
		this.subscriberInfo = subscriberInfo;
	}
	public com.digitalml.healthcare.foundation.shared.AddressEHub getSubscriberAddress() {
		return subscriberAddress;
	}
	
	public void setSubscriberAddress(com.digitalml.healthcare.foundation.shared.AddressEHub subscriberAddress) {
		this.subscriberAddress = subscriberAddress;
	}
	public com.digitalml.healthcare.financial.claim.ClaimAmountSet getTotalAmounts() {
		return totalAmounts;
	}
	
	public void setTotalAmounts(com.digitalml.healthcare.financial.claim.ClaimAmountSet totalAmounts) {
		this.totalAmounts = totalAmounts;
	}
	public boolean getBehavioralHealthIndicator() {
		return behavioralHealthIndicator;
	}
	
	public void setBehavioralHealthIndicator(boolean behavioralHealthIndicator) {
		this.behavioralHealthIndicator = behavioralHealthIndicator;
	}
	public boolean getCDHPIndicator() {
		return cDHPIndicator;
	}
	
	public void setCDHPIndicator(boolean cDHPIndicator) {
		this.cDHPIndicator = cDHPIndicator;
	}
	public boolean getERISAIndicator() {
		return eRISAIndicator;
	}
	
	public void setERISAIndicator(boolean eRISAIndicator) {
		this.eRISAIndicator = eRISAIndicator;
	}
	public boolean getITSIndicator() {
		return iTSIndicator;
	}
	
	public void setITSIndicator(boolean iTSIndicator) {
		this.iTSIndicator = iTSIndicator;
	}
	public boolean getSplitClaimIndicator() {
		return splitClaimIndicator;
	}
	
	public void setSplitClaimIndicator(boolean splitClaimIndicator) {
		this.splitClaimIndicator = splitClaimIndicator;
	}
	public boolean getAdjustmentIndicator() {
		return adjustmentIndicator;
	}
	
	public void setAdjustmentIndicator(boolean adjustmentIndicator) {
		this.adjustmentIndicator = adjustmentIndicator;
	}
	public Integer getRevisionNumber() {
		return revisionNumber;
	}
	
	public void setRevisionNumber(Integer revisionNumber) {
		this.revisionNumber = revisionNumber;
	}
	public Date getLastProcessedTimestamp() {
		return lastProcessedTimestamp;
	}
	
	public void setLastProcessedTimestamp(Date lastProcessedTimestamp) {
		this.lastProcessedTimestamp = lastProcessedTimestamp;
	}
	public Date getLastUpdatedTimestamp() {
		return lastUpdatedTimestamp;
	}
	
	public void setLastUpdatedTimestamp(Date lastUpdatedTimestamp) {
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
	}
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getUse() {
		return use;
	}
	
	public void setUse(String use) {
		this.use = use;
	}
	public String getPriority() {
		return priority;
	}
	
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getFundsReservation() {
		return fundsReservation;
	}
	
	public void setFundsReservation(String fundsReservation) {
		this.fundsReservation = fundsReservation;
	}
	public List<String> getCondition() {
		return condition;
	}
	
	public void setCondition(List<String> condition) {
		this.condition = condition;
	}
	public List<String> getException() {
		return exception;
	}
	
	public void setException(List<String> exception) {
		this.exception = exception;
	}
	public String getSchoolName() {
		return schoolName;
	}
	
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Date getAccidentDateTime() {
		return accidentDateTime;
	}
	
	public void setAccidentDateTime(Date accidentDateTime) {
		this.accidentDateTime = accidentDateTime;
	}
	public String getAccidentType() {
		return accidentType;
	}
	
	public void setAccidentType(String accidentType) {
		this.accidentType = accidentType;
	}
	public List<String> getInterventionException() {
		return interventionException;
	}
	
	public void setInterventionException(List<String> interventionException) {
		this.interventionException = interventionException;
	}
	public List<String> getAdditionalMaterials() {
		return additionalMaterials;
	}
	
	public void setAdditionalMaterials(List<String> additionalMaterials) {
		this.additionalMaterials = additionalMaterials;
	}
	public String getActionCode() {
		return actionCode;
	}
	
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public String getActionDescription() {
		return actionDescription;
	}
	
	public void setActionDescription(String actionDescription) {
		this.actionDescription = actionDescription;
	}
}