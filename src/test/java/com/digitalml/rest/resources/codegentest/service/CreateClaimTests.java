package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Claims.ClaimsServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.CreateClaimInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ClaimsService.CreateClaimReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateClaimTests {

	@Test
	public void testOperationCreateClaimBasicMapping()  {
		ClaimsServiceDefaultImpl serviceDefaultImpl = new ClaimsServiceDefaultImpl();
		CreateClaimInputParametersDTO inputs = new CreateClaimInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		CreateClaimReturnDTO returnValue = serviceDefaultImpl.createclaim(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}